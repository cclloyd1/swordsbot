FROM python:3.10-alpine

ADD requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

ADD swordsbot /app/swordsbot

WORKDIR /app

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH="/app"

ENTRYPOINT ["python", "-m", "swordsbot"]


