import logging


# Filter to pad levelname with spaces on right without padding inside brackets
from swordsbot.constants import LOG_LEVEL


def fmt_filter(record):
    record.levelname = '[%s]' % record.levelname
    record.funcName = '[%s]' % record.funcName
    return True


logging.basicConfig(
    format='SwordsBot | %(asctime)s | %(levelname)-7s %(message)s',
    level=LOG_LEVEL,
    datefmt='%Y-%m-%d %H:%M:%S',
)
logger = logging.getLogger('swordsbot')
logger.addFilter(fmt_filter)
