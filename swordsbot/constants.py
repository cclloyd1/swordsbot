import logging
from pathlib import Path

import praw
from environs import Env


env = Env()


DEBUG = env.bool('DEBUG', False)
DATA_DIR: Path = env.path('DATA_DIR', '/data')
DB_HOST = env.str('DB_HOST', 'localhost')
DB_PORT = env.int('DB_PORT', 5432)
DB_NAME = env.str('DB_NAME', 'swordsbot')
DB_USER = env.str('DB_USER', 'swordsbot')
DB_PASS = env.str('DB_PASS', 'swordsbot')
REDDIT_CLIENT = env.str('REDDIT_CLIENT', None)
REDDIT_SECRET = env.str('REDDIT_SECRET', None)
REDDIT_TOKEN = env.str('REDDIT_TOKEN', None)
DRY_RUN = env.bool('DRY_RUN', False)
INTERVAL = env.int('INTERVAL', 600)  # Interval to sleep for, in seconds
LOG_LEVEL = env.int('LOG_LEVEL', logging.INFO)


reddit = praw.Reddit(
    client_id=REDDIT_CLIENT,
    client_secret=REDDIT_SECRET,
    user_agent='python',
    username='SwordsBot',
    redirect_uri='http://localhost:8080',
    refresh_token=REDDIT_TOKEN,
)
