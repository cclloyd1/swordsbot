from typing import Optional
import html

import requests
import xmltodict
from bs4 import BeautifulSoup
import roman as roman_translator

from swordsbot.logger import logger


class WWWComic:
    title: Optional[str] = None
    roman: Optional[str] = None
    url: Optional[str] = None
    image_url: Optional[str] = None
    reddit_comics_id: Optional[str] = None
    reddit_swords_id: Optional[str] = None
    flavor: Optional[str] = None
    number: Optional[int] = None

    def __init__(self, title=None, roman=None, url=None, image_url=None, reddit_comics_id=None, reddit_swords_id=None, flavor=None, number=None):
        self.title = title.split('-')[-1].strip() if title else None
        self.roman = roman if roman else title.split('-')[0].strip() if title else None
        self.url = url
        self.image_url = image_url
        self.reddit_comics_id = reddit_comics_id
        self.reddit_swords_id = reddit_swords_id
        self.flavor = flavor
        self.number = number if number else roman_translator.fromRoman(roman)

    def __str__(self):
        return f'ID: {self.roman}, NUMBER: {self.number}, TITLE: {self.title}, FLAVOR: {self.flavor}\n' \
               f'URL: {self.url}\n' \
               f'IMAGE_URL: {self.image_url}'

    @staticmethod
    def newest():
        response = requests.get('https://swordscomic.com/comic/feed/')
        content = response.content
        xml_entry = xmltodict.parse(content)
        entry = xml_entry['rss']['channel']['item'][0]
        print(entry)
        entry_title = html.unescape(entry['title'])
        entry_url = entry['link']

        soup = BeautifulSoup(entry['description'], features="html.parser")
        entry_image = soup.findAll('img')[0]['src']

        return WWWComic(
            title=entry_title,
            roman=entry_title.split(' ')[0],
            url=entry_url,
            image_url=entry_image,
            flavor=get_flavor_text(entry_title.split(' ')[0]),
        )


def get_flavor_text(comic_id=None):
    if comic_id is None:
        raise ValueError('Must provide comic ID')
    comic_url = f'https://swordscomic.com/comic/{comic_id}/'

    def fetch_flavor_html(url):
        logger.info(f'Fetching flavor for: {url}')
        response = requests.get(url)
        content = response.content
        soup = BeautifulSoup(content, features="html.parser")
        return soup.find(id='comic-image')

    soup_contents = fetch_flavor_html(comic_url)
    attempts = 0
    last_in_series = False
    while not last_in_series:
        comic_url = f'https://swordscomic.com/comic/{comic_id}-{attempts+1}/'
        fetched = fetch_flavor_html(comic_url)
        if fetched:
            soup_contents = fetched
        else:
            last_in_series = True

        attempts += 1
        if attempts > 8:
            last_in_series = True



    flavor_text = soup_contents['title']
    logger.info(f'Flavor text: {flavor_text}')
    return flavor_text


def get_newest_comic():
    response = requests.get('https://swordscomic.com/comic/feed/')
    content = response.content
    xml_entry = xmltodict.parse(content)
    entry = xml_entry['rss']['channel']['item'][0]
    print(entry)
    entry_title = html.unescape(entry['title'])
    entry_url = entry['link']

    soup = BeautifulSoup(entry['description'], features="html.parser")
    entry_image = soup.findAll('img')[0]['src']

    comic = WWWComic(
        title=entry_title,
        roman=entry_title.split(' ')[0],
        url=entry_url,
        image_url=entry_image,
        flavor=get_flavor_text(entry_title.split(' ')[0]),
    )
    return comic
