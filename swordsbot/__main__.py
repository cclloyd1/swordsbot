import sys
from time import sleep

import swordsbot
from swordsbot.logger import logger
from .bot import SwordsBot

bot = SwordsBot()


def finish(signum, frame):
    logger.info('Exiting application...')
    sys.exit(0)


if __name__ == '__main__':
    print("Running module...")
    try:
        #bot.init()
        bot.main()
    except KeyboardInterrupt:
        sys.exit(0)
    except Exception as e:
        logger.fatal(e)
        sys.exit(1)




