import html
import logging
from pathlib import Path
from typing import Optional

import requests
import xmltodict
from bs4 import BeautifulSoup
from praw.models import Message, Submission
from praw.models.util import stream_generator

from swordsbot.constants import DATA_DIR, reddit, DRY_RUN
from swordsbot.logger import logger
import sys

from swordsbot.www import WWWComic


def superscript_text(text, level=1):
    text_array = text.split(' ')
    text_array_modified = []
    for item in text_array:
        text_array_modified.append(f'{"^" * level}{item}')
    return ' '.join(text_array_modified)


def has_commented(submission):
    logger.info(f'Checking if commented in {submission.id}')
    for comment in submission.comments:
        if comment.author == 'SwordsBot':
            return comment
    return None


class ComicProcessor:
    comment_template_base = 'Flavor text for [Swords %s](%s)\n> %s\n\n&nbsp;\n\n'
    comment_template = 'Read more at r/SwordsComic, on [Tapas.io](https://tapas.io/series/SwordsComic) or [see Quest Sprout early on Patreon](https://www.patreon.com/swordscomic).'
    comment_template_id = None
    comment_template_time = None

    comic: Optional[WWWComic] = None
    roman: Optional[str] = None
    reddit_id: Optional[str] = None
    number: Optional[int] = None
    matches: bool = False
    is_newest: bool = False
    subreddit = None
    submission: Submission = None
    comment = None

    def __init__(self):
        self.load_comment_file()
        self.comic = WWWComic.newest()

    def load_comment_file(self):
        # Prepare comment template from file
        comment_file = DATA_DIR / 'comment.txt'
        if comment_file.exists():
            logger.info('Fetching comment template from file...')
            with open(comment_file, 'r') as f:
                self.comment_template_id, self.comment_template_time = f.readline().strip().split(',')
                self.comment_template = f.read()
        self.update_comment_template()
        logging.debug(self.comment_template_id, self.comment_template_time, self.comment_template)

    def write_comment_file(self, new_comment: Message):
        if new_comment.id == self.comment_template_id:
            return

        comment_file = DATA_DIR / 'comment.txt'
        if not comment_file.exists():
            comment_file.parent.mkdir(parents=True, exist_ok=True)
            logger.info('Writing new comment file...')
            with open(comment_file, 'w') as f:
                f.write(f'{new_comment.id},{new_comment.created_utc}\n')
                f.write(new_comment.body)
            self.comment_template = new_comment.body
            self.comment_template_id = new_comment.id
            self.comment_template_time = new_comment.created_utc

    def update_comment_template(self):
        for item in reddit.inbox.messages():
            print(f'{item.was_comment} {item.author.name}')
            if item.author.name in ['MurkyWay', 'cclloyd'] and item.subject == 'update_template':
                self.write_comment_file(item)
                break

    def stream_inbox(self):
        for item in reddit.inbox.stream():
            print(item.author.name)
            if item.author.name in ['MurkyWay', 'cclloyd'] and item.subject == 'update_template':
                self.write_comment_file(item)
                break

    def get_newest_reddit_swords(self):
        for post in reddit.redditor('MurkyWay').submissions.new(limit=10):
            if not post.distinguished and not post.pinned and not post.stickied:
                if post.subreddit.display_name.lower() == 'swordscomic':
                    logger.debug(f'r/SwordsComic post: {post} - {post.title}')
                    if post.title.startswith('Swords ') or post.title.startswith('D') or post.title.startswith('C') or post.title.startswith('D') or post.title.startswith('M'):
                       logger.info(f'Found post: {post} - {post.title}')
                       return post
        return None

    def get_newest_reddit_comics(self):
        for post in reddit.redditor('MurkyWay').submissions.new(limit=10):
            if not post.distinguished and not post.pinned and not post.stickied:
                if post.subreddit.display_name.lower() == 'comics':
                    logger.info(f'r/Comics: Found post: {post} - {post.title}')
                    return post
        logger.error('No comic found in recent posts')
        return None

    def process_swords_subreddit(self):
        self.submission = self.get_newest_reddit_swords()
        self.is_newest = self.check_if_newest()
        if self.is_newest:
            self.comment = has_commented(self.submission)
        self.post(subreddit='SwordsComic')

    def process_comics_subreddit(self):
        self.submission = self.get_newest_reddit_comics()
        self.is_newest = self.check_if_newest()
        if self.is_newest:
            self.comment = has_commented(self.submission)
        self.post(subreddit='Comics')

    def post(self, subreddit=None):
        logger.info(f'Posting {subreddit} - is_newest:{self.is_newest} - comment:{self.comment}')
        if self.is_newest:
            if not self.comment:
                logger.info(f'r/{self.subreddit}: Not commented')
                content = self.comment_template_base % (self.comic.roman, self.comic.url, self.comic.flavor)
                content += self.comment_template
                logger.debug(content)
                if not DRY_RUN:
                    logger.info('Actually posting comment.')
                    self.comment = self.submission.reply(content)
                else:
                    logger.info(f'Dry run. Skipping posting comment on {subreddit}')
                return self.comment
            else:
                logger.debug('Already commented')

    def process(self):
        self.process()

    def check_if_newest(self):
        title = self.submission.title.split('~')[-1].strip()
        if title == self.comic.title:
            return True
        if title == self.comic.title.split('~')[-1].strip():
            return True
        return False
