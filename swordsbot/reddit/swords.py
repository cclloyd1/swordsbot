from typing import Optional

import roman as roman_translator
from praw.reddit import Submission

from swordsbot.constants import DRY_RUN, reddit
from swordsbot.logger import logger
from swordsbot.reddit import has_commented, ComicProcessor
from swordsbot.www import WWWComic


def get_newest_reddit_swords():
    for post in reddit.redditor('MurkyWay').submissions.new(limit=10):
        if post.subreddit.display_name.lower() == 'swordscomic':
            print(post.title)
            if not post.distinguished and not post.pinned and not post.stickied:
                logger.debug(f'r/SwordsComic post: {post} - {post.title}')
                if post.title.startswith('Swords ') or post.title.startswith('D') or post.title.startswith('C') or post.title.startswith('D') or post.title.startswith('M'):
                    logger.info(f'Found post: {post} - {post.title}')
                    return post
    return None


class SwordsComic(ComicProcessor):
    comic: Optional[WWWComic] = None
    roman: Optional[str] = None
    reddit_id: Optional[str] = None
    number: Optional[int] = None
    matches: bool = False
    is_newest: bool = False
    subreddit: 'SwordsComic'

    #def __init__(self, *args, **kwargs):
    #    super().__init__(*args, **kwargs)

    def __init__(self, comic, title=None, roman=None, image_url=None, reddit_id=None, number=None):
        self.comic = comic
        self.title = title
        self.roman = roman
        self.image_url = image_url
        self.reddit_id = reddit_id
        self.is_newest = False
        self.number = number if number else (roman_translator.fromRoman(roman) if roman else None)
        self.comment = None

        self.submission: Submission = get_newest_reddit_swords()

        self.is_newest = self.check_if_newest()
        if self.is_newest:
            self.comment = has_commented(self.submission)

    def __str__(self):
        return f'[SWORDS] ID: {self.roman}, NUMBER: {self.number}, TITLE: {self.title}, REDDIT_ID: {self.reddit_id}'
