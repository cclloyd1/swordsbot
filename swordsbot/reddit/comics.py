import json
from pprint import pprint
from typing import Optional

from praw.models import Submission
from roman import fromRoman

from swordsbot.constants import reddit, DRY_RUN
from swordsbot.logger import logger
from swordsbot.reddit import has_commented, ComicProcessor
from swordsbot.www import WWWComic


def get_newest_reddit_comics():
    for post in reddit.redditor('MurkyWay').submissions.new(limit=10):
        logger.debug(f'{post} - {post.subreddit.display_name.lower()} - {post.title}')
        if not post.distinguished and not post.pinned and not post.stickied:
            if post.subreddit.display_name.lower() == 'comics':
                logger.info(f'r/Comics: Found post: {post} - {post.title}')
                return post
    logger.error('No comic found in recent posts')
    return None


class ComicsComic(ComicProcessor):
    comic: Optional[WWWComic] = None
    roman: Optional[str] = None
    reddit_id: Optional[str] = None
    number: Optional[int] = None
    is_newest: bool = False
    subreddit: 'Comics'

    def __init__(self, comic, title=None, roman=None, image_url=None, reddit_id=None, number=None):
        self.comic = comic
        self.title = title
        self.roman = roman
        self.image_url = image_url
        self.reddit_id = reddit_id
        self.is_newest = False
        self.number = number if number else (fromRoman(roman) if roman else None)
        self.comment = None

        self.submission: Submission = get_newest_reddit_comics()

        self.is_newest = self.check_if_newest()
        if self.is_newest:
            self.comment = has_commented(self.submission)

    def __str__(self):
        return f'[COMICS] ID: {self.roman}, NUMBER: {self.number}, TITLE: {self.title}, REDDIT_ID: {self.reddit_id}'
