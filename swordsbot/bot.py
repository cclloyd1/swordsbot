import traceback
from time import sleep

from swordsbot.constants import INTERVAL
from swordsbot.logger import logger
from swordsbot.reddit import ComicProcessor
from swordsbot.reddit.comics import ComicsComic
from swordsbot.reddit.swords import SwordsComic
from swordsbot.www import get_newest_comic
import sys


class SwordsBot:
    newest = None

    def init(self):
        self.newest = get_newest_comic()

    def main(self):
        while True:
            try:
                p = ComicProcessor()
                p.update_comment_template()


                p.process_swords_subreddit()
                p.process_comics_subreddit()
                sys.exit(0)
            except Exception as e:
                logger.error(f'FATAL ERROR: {e}')
                logger.error(traceback.print_exc())
            logger.info(f'Sleeping for {INTERVAL} seconds')
            sleep(INTERVAL)
